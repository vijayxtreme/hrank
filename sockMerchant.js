function sockMerchant(n, ar) {
    let pairCount = 0;
    let cache = {};
    for (let i = 0; i <= n; i++) {
        let num = ar[i];
        if (!cache[num]) {
            cache[num] = num;
        } else {
            pairCount++;
            delete cache[num];
        }
    }
    return pairCount;
}

let res = sockMerchant(10, [1, 1, 3, 5, 2, 1, 2])
console.log(`There are ${res} pairs of socks`)