    //we could break this into something simpler

    //ratios aren't accurate.
    //maybe we could loop through the string
    //find the instances of a, then 
    //figure out how many times we have to 
    //append the string for count n

    /* so ex:

        n = 57
        found 2 instances of a in sample string s of length 5
        then we have 57 - 5 = 52 more letters to tack on

        if we know a is in our sequence of 10 letters, 2x
        then a would again appear 2x more in the next 5 letter seq
        or 2 every 5.  

        For every 5 iterations, 2 will be 'a'
  
        so if string is length 100
        n = 300
        a appears 30 times, for every 100, a is there 30.
        so again, (30 x 300) / 100 =  90 times.


    */


function repeatedString(s, n){
    
   
    let a = 0;
    //find occurrences of a in s
    for(let i=0; i<s.length; i++){
        if(s[i]=='a'){
            a++;
        }
    }  

    //pattern repeats n times
    //how many patterns?
    let g = Math.floor(n / s.length);
    let r = n % s.length;
    let res = (a*g);
    
    for(let i=0; i<r; i++){
        if(s[i]=='a'){
            res++;
        }
    }
    

    console.log(res);



}

let res = repeatedString('epsxyyflvrrrxzvnoenvpegvuonodjoxfwdmcvwctmekpsnamchznsoxaklzjgrqruyzavshfbmuhdwwmpbkwcuomqhiyvuztwvq', 549382313570);
