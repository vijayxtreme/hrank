/* 
    pseudocode:
    s = a string to repeat
    n = number of chars to consider

    s between 1 - 100 length
    n between 1 to 10^12 *trillion* 

    - take a string, n characters to consider (loop)
    - we need to repeat the string however, for n characters
    - this is another loop
    - after doing this
    - loop over the new string, find instances of 'a'
    - count, keep track
    - give back the count

    - loop could be very long (trillion), any way to
    - shorten the time? (maybe consider square root)
    - other tactics?

    - can we cache the looping, so it's not O(n),
    - like that one technique from the book
    - where it's comparing one to the last one only?


*/

function repeatedString(s, n){
    //get length of s
    
    //create the infinite string:
    //we need to repeat this string of length l
    //until we have a string of n length
    //n - l equals required repeat length
    //ex: n = 10, l = 4, so 6 character spaces avail
    //sample input = 'abcd', then we'd need to add 'abcd'
    //over and over until we are at n-l length
    function createInfiniteString(){
        let l = s.length;
        let r = n - l; //remaining length
        let nS = s //new copy string to evaluate
        //console.log(nS)
    
        let i=0;
        let j = 0;
        while(i<r){
            if(s[j]){
                nS+=s[j]
                j++;
            }else {
                j=0;
                nS+=s[j];
                j++;
            }
            i++;
        }
        return nS;
    }

    function countInstancesOfA(s, n){
        let nS = createInfiniteString();
        let c = 0; //count
    
        for(let i = 0; i<n; i++){
            if(nS[i]=='a') c++;
        }
        return c;
    }
    return countInstancesOfA(s, n);
}

let res = repeatedString('a', 1000000);
console.log(res);