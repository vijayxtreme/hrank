    //we could break this into something simpler

    //we know the string is going to be some combination of
    //letters abc up to 100.  so we could count this first
    //using math, we could find out how often this
    //has to be repeated by breaking up the string

    //ex: abc
    //n is 10000
    //so abc is length 3
    //a is 33.3% of the total, so abc comprises of 99%, with 1 left over for
    //another a.  we need to keep track of the total, and remainder
    //then we can iterate once more for the remainder to get the next char


function repeatedString(s, n){
    function getCountOfA(){
        let c = 0; //count instances of a here
        let o = 0; //remaning chars
       
        for(let i=0; i<s.length; i++){
            if(s[i]=='a'){ 
                c++; // instances of a
            }else {
                o++;
            }
        }

        //there were c instances of a

        let t = c + o  //ratio of a to remaining chars
        let r = c/t;
        console.log(r)

        let rem = t%c; //any remainders?

        let res = countRemainderAs(rem);
        let my = { ratio: r, remainder: res}
        return my;
    }

    function countRemainderAs(rem){
        let c = 0;
        if(rem){
            //iterate through string rem times, is there an a?
            for(let i=0; i<rem; i++){
                console.log(s[i])
                if(s[i]=='a'){
                    c++;
                }
            }
        }
        return c;
    }

    if(s.length > 1){
        let my = getCountOfA();        console.log(my);
        let res = Math.trunc((my.ratio * n) + my.remainder);
        console.log(`Found ${res} a's`)
         return res;
        
        
    }else if(s.length == 1) {
        if(s[0]=='a'){
            return n;
        }else {
            console.log(`No a's found.`);
            return 0;
        }        
    }else {
        console.log(`String length was less than 1`);
        return 0;
    }
}

let res = repeatedString('epsxyyflvrrrxzvnoenvpegvuonodjoxfwdmcvwctmekpsnamchznsoxaklzjgrqruyzavshfbmuhdwwmpbkwcuomqhiyvuztwvq', 549382313570);
