function jumpingOnClouds(n) {
    let badClouds = [];
    let count = 0;
    for (let i = 0; i <= n.length; i++) {
        //find out where bad elements are
        if (n[i] == 1) {
            badClouds.push(i) //index 4

        }
    }
    // console.log(badClouds)
    let j = 0;
    for (let i = 0; i < badClouds.length; i++) {

        while (j <= badClouds[i]) {
            // console.log(badClouds[i], j)
            if (badClouds[i] == j + 1) {
                //if next index is bad
                //must jump 2 steps
                j += 2;
                count++;
                //console.log("Must jump two steps")
            } else if (badClouds[i] - 1 - j >= 2) {
                //safe to jump 2 steps
                // console.log("Jumped two steps")
                j += 2;
                count++;
            } else if (badClouds[i] - 1 - j >= 0) {
                //jump 1 step
                j++;
                count++;
                //console.log("Jumped one step")

            } else {
                //probably done with current bad cloud, go to next 
                j++;
            }
        }
    }
    //one more for loop if there are any 0's left
    //can keep jumping 2's
    j++;
    while (j < n.length) {
        console.log(n[j], j)
        count++;
        j += 2;
    }


    return count;
}
let arr = ["0", "1", "0", "0", "0", "0", "0", "1", "0", "1", "0", "0", "0", "1", "0", "0", "1", "0", "1", "0", "0", "0", "0", "1", "0", "0", "1", "0", "0", "1", "0", "1", "0", "1", "0", "1", "0", "0", "0", "1", "0", "1", "0", "0", "0", "1", "0", "1", "0", "1", "0", "0", "0", "1", "0", "1", "0", "0", "0", "1", "0", "1", "0", "0", "0", "1", "0", "0", "1", "0", "1", "0", "1", "0", "1", "0", "1", "0", "1", "0", "1", "0", "0", "1", "0", "1", "0", "1", "0", "1", "0", "0", "0", "0", "0", "0", "1", "0", "0", "0"]
let res = jumpingOnClouds(arr)
console.log(`The count is ${res}`)

//[0, 0, 0, 0, 1, 0]
//[0, 0, 1, 0, 0, 1, 0]
//[0, 0, 0, 1, 0, 0]